const express = require("express");
const bodyParser = require('body-parser');
const cors = require('cors');
const database = require("./src/utils/database");
const routes = require("./src/routes");
const helmet = require("helmet");
const path = require('path');

const app = express(); 

// Load environment variables
require('dotenv').config();

// Middleware setup
app.use(bodyParser.json());
app.use(express.json());
app.use(helmet());
app.use(cors());
// Database connection
database();

// Determine base URL dynamically
const baseURL = process.env.BASE_URL || 'http://localhost:3000';

// CORS setup for API routes
app.use('/api', cors({
    origin: baseURL,
    methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
    allowedHeaders: ['Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'Authorization'],
    credentials: true
}));

// Serve static files (images) from /tmp/uploads
app.use('/images', express.static(path.join(__dirname, 'tmp', 'uploads')));

// Routes setup
app.use("/api", routes); 

// CORS preflight handling
app.options("/*", cors());

// Error handling middleware
app.use((req, res, next) => {
    res.status(404).send('Not Found');
});

// Server listening
const port = process.env.PORT || 3001; // Use PORT from environment variables or default to 3001
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
