const express = require('express');
const router = express.Router();

const vendorController =require('../../controllers/vendor')

router.get('/productsbyvendor/:id', (req, res) => {
    const userId = req.params.id;
    vendorController.getProductsByUserId(userId, res);
});
 
router.get('/getquantitybyid/:id/:vendorId', (req, res) => {
    const productId = req.params.id;
    const vendorId = req.params.vendorId;
    vendorController.getpNumberById(productId,vendorId, res);
});
router.post('/placeorder', vendorController.placeOrder)
router.get('/orders/:id',vendorController.getOrdersById)
 module.exports = router
