const express = require('express');
const router = express.Router();
const productsController = require('../../controllers/products');
const { put } = require('@vercel/blob');
const API_URL = process.env.REACT_APP_IMG_URL;
const REACT_APP_API_BLOB_READ_WRITE_TOKEN = process.env.REACT_APP_API_BLOB_READ_WRITE_TOKEN;

const uploadImage = async (image) => {
  const { url } = await put(`images/${image.name}`, image.data, {
    access: 'public',
    token: REACT_APP_API_BLOB_READ_WRITE_TOKEN,
  });
  return url;
};

router.post('/add', async (req, res) => {
  const image = req.file;
  if (image) {
    const imageUrl = await uploadImage(image);
    req.body.image = imageUrl;
  }
  productsController.addproducts(req.body, res);
});

 router.get('/byproductid/:id', (req, res) => {
    const productId = req.params.id;
    productsController.getProductsById(productId, res);
});


 router.get('/getAllProducts',productsController.getAllProducts)

 router.get('/byuserid/:id', (req, res) => {
    const userId = req.params.id;
    productsController.getProductsByUserId(userId, res);
});



router.get('/pnumberbyid/:id', (req, res) => {
    const productId = req.params.id;
    productsController.getpNumberById(productId, res);
});

router.put('/update',  productsController.updateProduct);

 module.exports = router
