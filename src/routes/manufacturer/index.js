

const express = require('express');
const router = express.Router();

const { getAllManufacturers, getManufacturerById } = require('../../controllers/manufacturer');

//router.get('/manufacturer/:id', getManufacturerById);
router.get('/manufacturer/:id', (req, res) => {
    const id = req.params.id;
    getManufacturerById(id, res);
});
router.get('/allmanufacturers', getAllManufacturers);

module.exports = router;


