const express = require('express');
const router = express.Router();


const accountController =require('../../controllers/account')

router.post('/login',accountController.login)
router.post('/signup',accountController.signUp)
router.post('/resetpassword',accountController.resetPassword)
module.exports = router;