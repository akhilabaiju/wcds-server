const mongoose = require('mongoose');

// Define schema for Address
const addressSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  addressLine1: {
    type: String,
    required: true
  },
  addressLine2: String,
  city: {
    type: String,
    required: true
  },
  state: {
    type: String,
    required: true
  },
  postalCode: {
    type: String,
    required: true
  },
  country: {
    type: String,
    required: true
  },
  phoneNumber: {
    type: String,
    required: true
  },
  isDefault: {
    type: Boolean,
    default: false
  }
});

// Create model for Address
const Address = mongoose.model('Address', addressSchema);

module.exports = Address;
