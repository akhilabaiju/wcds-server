require('dotenv').config(); // Load dotenv

const mongoose = require('mongoose');

const connect = async () => {
  try {
    const options ={useNewURLParser:true,}
    const uri = process.env.MONGODB_URI; // Load MongoDB URI from environment variables
    await mongoose.connect(uri, options);
    console.log('Connected to MongoDB');
  } catch (error) {
    console.error('Error connecting to MongoDB:', error.message);
    process.exit(1); // Exit the application on connection failure
  }
}; 

module.exports = connect;
