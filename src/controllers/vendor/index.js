const VendorProductsData = require('../../models/VendorProductsData');
const OrderData =require("../../models/Order")
const VendorOrderData =require("../../models/VendorOrder")
const getProductsByUserId = async (id, res) => {
  try {

    const products = await VendorProductsData.find({ userId: id });

    if (!products || products.length === 0) {
        return res.json(products);
    } else {
      
        return res.json(products); 
    }
    
  } catch (error) {
      console.error('Error fetching products data:', error);
      res.status(500).json({ message: 'Internal server error' });
  } 
}; 
 
 const getpNumberById = async (productId,vendorId, res) => {
  try {
   
    const products = await VendorProductsData.find({ productId: productId,userId:vendorId });
    if (!products || products.length === 0) {
      return res.status(404).json({ message: 'No Products found' });
    } else {
     
      res.json(products);
    } 
  } catch (error) {
    console.error('Error fetching products data:', error);
    res.status(500).json({ message: 'Internal server error' });
}
 }

 const placeOrder = async (req, res) => {
  try {
      const cart = req.body;
      
          const newOrderItem = new OrderData({
              manufactureId: cart[0].manufactureId,
              userId: cart[0].userId,
              vendorId: cart[0].vendorId,
              productId: cart[0].productId,
              quantity: cart[0].quantity,
              amount: cart[0].quantity * cart[0].productPrice, 
              totalAmount: (cart[0].quantity * cart[0].productPrice) + 50, 
          });

          // Save the new order item to the database
          await newOrderItem.save();

          const newOrder = new VendorOrderData({
            manufactureId: cart[0].manufactureId,
            userId: cart[0].userId,
            vendorId: cart[0].vendorId,
            productId: cart[0].productId,
            quantity: cart[0].quantity,
            amount: cart[0].quantity * cart[0].productPrice, 
            totalAmount: (cart[0].quantity * cart[0].productPrice) + 50, 
        });
        await newOrder.save();
          // Update the product quantity
          const updatedProduct = await VendorProductsData.findOneAndUpdate(
              { productId: cart[0].productId,userId: cart[0].vendorId },
              { $inc: { pNumber: -cart[0].quantity } }, 
              { new: true } 
          );  



      res.status(200).json({ message: 'Order placed successfully' });
  } catch (error) {
      console.error('Error placing order:', error);
      res.status(500).json({ error: 'Internal server error' });
  }
};
const getOrdersById =  async (req, res) => {

  const id=req.params.id;
          try {
             const orders = await VendorOrderData.find({ vendorId: id });
            
                if (!orders) {
    return res.status(404).json({ message: 'orders not found' });
  }else{
   res.json(orders);
  }
} catch (error) {
  console.error('Error fetching user data:', error);
  res.status(500).json({ message: 'Internal server error' });
}
}

module.exports = {  getProductsByUserId, getpNumberById, placeOrder, getOrdersById};

