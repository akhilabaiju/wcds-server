
const ProductsData = require('../../models/Productdata');
 
 
 const getAllProducts = async (req, res) => {

   
          try {
          const products = await ProductsData.find();
          
          if (!products) {
      return res.status(404).json({ message: 'No Products found' });
    }else{
     
           res.json(products);
    }
  } catch (error) {
    console.error('Error fetching products data:', error);
    res.status(500).json({ message: 'Internal server error' });
  }

}
//router.post('/add', upload.single('image'), async (req, res) => {
const addproducts = async (req, res) => {
  try {
   
    const newItem = new ProductsData({
    
     manufactureId:req.body.manufactureId,
     productName:req.body.productName,
     productWeight:req.body.productWeight,
     productPrice:req.body.productPrice ,
     pNumber:req.body.pNumber ,
      imageUrl: req.file.path
      
    });
   
    await newItem.save();
    res.status(201).send('Item created successfully');
  } catch (error) {
    console.error(error);
    res.status(500).send('Error creating item');
  }

}

const updateProduct = async (req,res) =>{
 
    const updatedUserData = req.body; 
   const userId =updatedUserData._id;
    try {
      const updatedUser = await ProductsData.findByIdAndUpdate(
        userId,
        { $set: updatedUserData },
        { new: true } // Return the modified document
      );
  
      if (!updatedUser) {
        return res.status(404).json({ message: 'User not found' });
      }
      res.json(updatedUser);
    } catch (error) {
      console.error('Error updating user:', error);
      res.status(500).json({ message: 'Internal server error' });
    }
}
const getProductsByUserId = async (id, res) => {
  try {
    
      const products = await ProductsData.find({ manufactureId: id });
      if (!products || products.length === 0) {
          return res.status(404).json({ message: 'No Products found' });
      } else {
          res.json(products);
      }
  } catch (error) {
      console.error('Error fetching products data:', error);
      res.status(500).json({ message: 'Internal server error' });
  } 
}; 

const getProductsById = async (productId, res) => {
  try {
    const products = await ProductsData.find({ _id: productId });
    if (!products || products.length === 0) {
      return res.status(404).json({ message: 'No Products found' });
    } else {
   
      res.json(products);
    } 
  } catch (error) {
    console.error('Error fetching products data:', error);
    res.status(500).json({ message: 'Internal server error' });
}
 }; 
 const getpNumberById = async (productId, res) => {
  try {
    
    
  } catch (error) {
    console.error('Error fetching products data:', error);
    res.status(500).json({ message: 'Internal server error' });
}
 }
module.exports = { getAllProducts,addproducts, getProductsByUserId, getProductsById,updateProduct ,getpNumberById};

