
const CartItem = require("../../models/CartItem")



const addToCart = async (req, res) => { 
    try {

        const { manufactureId ,userId ,vendorId,productId ,quantity, productName,productWeight,
        productPrice,imageUrl } = req.body; 
        
      const newCartItem = new CartItem({
        manufactureId ,userId ,vendorId,productId ,quantity, productName,productWeight,productPrice,imageUrl
      });
     
      await newCartItem.save();
      res.status(201).json({ message: 'Cart item added successfully' });
    } catch (error) {
      console.error('Error adding cart item:', error);
      res.status(500).json({ error: 'Internal server error' });
    }
  }

const updateCart = async (req, res) => {
    try {
   
      const { manufactureId ,userId,vendorId ,productId ,quantity } = req.body; 

      const updatedCartItem = await CartItem.findOneAndUpdate(
        { userId: userId, productId: productId, vendorId: vendorId },
        { quantity: quantity },
        { new: true } // Return the updated document
      );
  
      if (!updatedCartItem) {
        return res.status(404).json({ error: 'Cart item not found' });
      }
  
      res.json(updatedCartItem);
    } catch (error) {
      console.error('Error updating cart:', error);
      res.status(500).json({ error: 'Internal server error' });
    }
  }

  const getItemQuantityInCart = async (userId,productId,vendorId, res) => { 
    try {
   
      const cartItem = await CartItem.findOne({ userId: userId, productId: productId,vendorId:vendorId });
 
      if (cartItem) {
        res.json({ quantity: cartItem.quantity }); // Send the quantity as JSON response
      } else {
        res.json({ quantity: 0 }); // Send 0 if cart item not found
      }
    } catch (error) {
      console.error('Error getting item quantity in cart:', error);
      res.status(500).json({ error: 'Internal server error' }); // Send 500 status with error message
    }
  }

  const getCartById = async (id,vendorId, res) => {
   
 
         try {
     
         const cartItems = await CartItem.find({userId:id,vendorId:vendorId});
      
          if (!cartItems) {
      return res.status(404).json({ message: 'No Products found' });
    }else{
  
    res.json(cartItems);
    }
  } catch (error) {
    console.error('Error fetching products data:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
  }

  const removeCart = async(cartItemId,vendorId,res) => {
    
     
    try {
    const result= await CartItem.findByIdAndDelete(cartItemId);
    res.json({ message: 'Field updated successfully', result });
    } catch (error) {
      console.error('Error updating document:', error);
    }
 
   }
module.exports = { addToCart, updateCart ,getItemQuantityInCart, getCartById, removeCart};

