
const CartItem = require('../../models/CartItem');

const ProductsData = require('../../models/Productdata');
const OrderData = require("../../models/Order");
const VendorProductsData =  require("../../models/VendorProductsData")

const placeOrder = async (req, res) => {
    try {
        const cart = req.body;
        for (const item of cart) {
            // Validate item data before processing
            if (!isValidOrderItem(item)) {
                return res.status(400).json({ error: 'Invalid order item data' });
            }

            // Create a new order item
            const newOrderItem = new OrderData({
                manufactureId: item.manufactureId,
                userId: item.userId,
                vendorId: "No Vendor",
                productId: item.productId,
                quantity: item.quantity,
                amount: item.quantity * item.productPrice, 
                totalAmount: (item.quantity * item.productPrice) + 50, 
            });

            // Save the new order item to the database
            await newOrderItem.save();

            // Update the product quantity
            const updatedProduct = await ProductsData.findOneAndUpdate(
                { _id: item.productId },
                { $inc: { pNumber: -item.quantity } }, 
                { new: true } 
            );  

            // Remove the item from the cart
            const result = await CartItem.findByIdAndDelete(item._id);
        }

        res.status(200).json({ message: 'Order placed successfully' });
    } catch (error) {
        console.error('Error placing order:', error);
        res.status(500).json({ error: 'Internal server error' });
    }
};


const placeOrderv = async (req, res) => {
  try {
      const cart = req.body;
          const newOrderItem = new OrderData({
              manufactureId: cart.manufactureId,
              userId: cart.userId,
              vendorId: cart.userId,
              productId: cart.productId,
              quantity: cart.quantity,
              amount: cart.quantity * cart.productPrice, 
              totalAmount: (cart.quantity * cart.productPrice) + 50, 
          });

          // Save the new order item to the database
          await newOrderItem.save();
          const newItem = new VendorProductsData({
    
            manufactureId:cart.manufactureId,
            userId: cart.userId,
            productId: cart.productId,
            productName:cart.productName,
            productWeight:cart.productWeight,
            productPrice:cart.productPrice ,
            pNumber:cart.quantity ,
            imageUrl: cart.imageUrl
             
           });
          
           await newItem.save();

          // Update the product quantity
          const updatedProduct = await ProductsData.findOneAndUpdate(
              { _id: cart.productId },
              { $inc: { pNumber: -cart.quantity } }, 
              { new: true } 
          );  



      res.status(200).json({ message: 'Order placed successfully' });
  } catch (error) {
      console.error('Error placing order:', error);
      res.status(500).json({ error: 'Internal server error' });
  }
};



// Utility function to validate order item data
const isValidOrderItem = (item) => {
    // Your validation logic here
    // Example: Check if all required fields are present and have valid values
    return true; // Replace with actual validation logic
};


const getOrdersById =  async (req, res) => {
    
    const id=req.params.id;
            try {
               const orders = await OrderData.find({ userId: id });
                  if (!orders) {
      return res.status(404).json({ message: 'orders not found' });
    }else{
     res.json(orders);
    }
  } catch (error) {
    console.error('Error fetching user data:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
  }


const getManufactureOrders = async (req, res) => {
   
    const id=req.params.id;
            try {
        const orders = await OrderData.find({ manufactureId: id });
          if (!orders) {
      return res.status(404).json({ message: 'orders not found' });
    }else{
   
    res.json(orders); 
    }
  } catch (error) {
    console.error('Error fetching user data:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
}
 
module.exports = {placeOrder, getOrdersById, getManufactureOrders,placeOrderv };



