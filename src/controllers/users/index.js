
const UsersData = require("../../models/Userdata");
const AddressData =require("../../models/Address")


 const getAllUsers = async (req, res) => {

  
          try {
          const users = await UsersData.find();
          
          if (!users) {
      return res.status(404).json({ message: 'No Products found' });
    }else{
           res.json(users);
    }
  } catch (error) {
    console.error('Error fetching products data:', error);
    res.status(500).json({ message: 'Internal server error' });
  }

}
const getVendors = async (req, res) => {

 
        try {
       
        const vendors=await UsersData.find({ userType: "vendor" })
    
        if (!vendors) {
           return res.status(404).json({ message: 'No vendors found' });
        }else{
         res.json(vendors);
  }
} catch (error) {
  console.error('Error fetching products data:', error);
  res.status(500).json({ message: 'Internal server error' });
}

}

const getUserById = async (id, res) => {
  try {
    
    const user = await UsersData.findById(id);
     if (!user) {
 return res.status(404).json({ message: 'User not found' });
}else{
 res.json(user);
}
} catch (error) {
console.error('Error fetching user data:', error);
res.status(500).json({ message: 'Internal server error' });
}
};

 const updateUser =async(req,res) => { 
  const updatedUserData = req.body; 
  const id= updatedUserData._id;

try {
  const updatedUser = await UsersData.findByIdAndUpdate(
    id,
    { $set: updatedUserData },
    { new: true } // Return the modified document
  );
  if (!updatedUser) {
    return res.status(404).json({ message: 'User not found' });
  }
  res.json(updatedUser);
} catch (error) {
  console.error('Error updating user:', error);
  res.status(500).json({ message: 'Internal server error' });
}
}
const addAddress = async(req,res) => {
  try {
    const address = new AddressData(req.body);
   // const userId=req.body.userId
    await address.save();
    res.status(201).json({ message: 'Address added successfully', address });
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
 }

  // const getAddressByUserId = async(req,res) => {
  //   try {
  //     const addresses = await AddressData.find({ userId: req.params.userId });
  //     res.status(200).json({ addresses });
  //   } catch (error) {
  //     res.status(500).json({ error: error.message });
  //   }
  // }

  const getAddressByUserId = async (userId, res) => {
    try {
       
        if (!isValidUserId(userId)) {
            return res.status(400).json({ error: 'Invalid user ID format' });
        }

        // Fetch addresses for the user
        const addresses = await AddressData.find({ userId });
        res.status(200).json({ addresses });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// Utility function to validate user ID format
const isValidUserId = (userId) => {
    // Your validation logic here
    // Example: Check if userId is a valid ObjectId if using MongoDB
    return true; // Replace with actual validation logic
};

  const getAddressById =async (req, res) => {
    try {
      const address = await AddressData.findById(req.params.id);
       res.status(200).json({ address });
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }

  // const UpdateAddress = async (req, res) => {
  //   try {
  //     const address = await AddressData.findByIdAndUpdate(req.params.id, req.body, { new: true });
  //     if (!address) {
  //       return res.status(404).json({ message: 'Address not found' });
  //     }
  //     res.status(200).json({ message: 'Address updated successfully', address });
  //   } catch (error) {
  //     res.status(400).json({ error: error.message });
  //   }
  // }

  const deleteAddress =async (req, res) => {
    try {
      const address = await AddressData.findByIdAndDelete(req.params.id);
      if (!address) {
        return res.status(404).json({ message: 'Address not found' });
      }
      res.status(200).json({ message: 'Address deleted successfully' });
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  }
module.exports = {getAllUsers, getUserById, updateUser, addAddress, getAddressByUserId, getAddressById , deleteAddress, getVendors};

