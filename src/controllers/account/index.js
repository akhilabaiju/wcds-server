
const UsersData = require("../../models/Userdata");
 

 const login = async (req, res) => {
 
   const useremail = req.body.email;
   const userpassword = req.body.password;

    try{
          const user = await UsersData.findOne({email : useremail});
           if(user){
              const result = (userpassword === user.password);
              if (result) {
                var id=user._id;
                var type=user.userType;
                res.json({userType : type, userId : id})
                } 
                else {
                        res.status(400).json({ error: "password doesn't match." });
                     }
            } else {
                      res.status(400).json({ error: "User doesn't exist" });
                   }
    } catch (error) {
      res.status(400).json({ error });
    }
 
};

const signUp = async (req, res) => {
 
    const formData = req.body;
  var create = new UsersData(formData);
  try {
    if (formData.firstName == ('') || formData.lastName == ('') || formData.email == ('') || formData.password == ('') || formData.contactNumber == ('') || formData.userType == ('')) {
      res.send({ message: "Field cannot be empty" })
      //return res.status(400).json({ message: 'All fields are required' });
    } else {
      create.save();
      res.json({ message: 'Form data received successfully!' });
    }
  } catch (error) {
    console.error('Error registering user:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
}

const resetPassword = async (req, res) => {
    const formData = req.body;
  var create = new UsersData(formData);
  var email = formData.email;
  var newPassword =formData.password
 // console.log(create)
  try {
    if ( formData.email == ('') || formData.password == ('') ) {
      res.send({ message: "Field cannot be empty" })
      //return res.status(400).json({ message: 'All fields are required' });
    } else {
      await UsersData.updateOne(
        { email: email },
        { $set: { password: newPassword } }
      );
     // create.save();
      res.json({ message: 'Form data received successfully!' });
    }
  } catch (error) {
    console.error('Error registering user:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
}
module.exports = { login, signUp, resetPassword };

